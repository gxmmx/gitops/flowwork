README.md
CHANGELOG.md
CODE_OF_CONDUCT.md
CONTRIBUTING.md
LICENSE
CODEOWNERS
.gitignore

docs/
    page.md
    folder.md
    folder
        subpage.md

build/
    Dockerfile
    docker-config.yml

provision/
    linode/
        main.tf

deploy/
    inventory
        hosts.yml
    site.yml

.lint/
    lint-yaml.yml
    lint-ansible.yml
    lint-markdown.yml
    lint-fundamentals.yml
    lint-dockerfile.yml
