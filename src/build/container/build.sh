#!/usr/bin/env sh

set -e

# ------------------------------------------------------------------------------
# Functions
# ------------------------------------------------------------------------------

# Vars
build_version=${FLOW_RELEASE_VERSION:-$CI_COMMIT_SHORT_SHA}
pushrm_url="https://github.com/christian-korneck/docker-pushrm/releases/download"
buildx_url="https://github.com/docker/buildx/releases/download"

release_link_gitlab="link_container_build_gitlab.yml"
release_link_dockerhub="link_container_build_dockerhub.yml"

# Ensure dependencies
function ensure_dependencies() {
    if [ ! -f "$_flow_build_container_path" ]; then
        printf "ERROR: Buildfile '%s' missing\n" "$_flow_build_container_path"
        exit 1
    fi
}


# Install buildx
function setup_buildx() {
    if [ "$_flow_build_container_multiarch" == "true" ]; then
        BUILDX_VERSION="$_flow_build_container_buildx_version"
        mkdir -p /usr/lib/docker/cli-plugins
        wget -q -O /usr/lib/docker/cli-plugins/docker-buildx "${buildx_url}/v${BUILDX_VERSION}/buildx-v${BUILDX_VERSION}.linux-amd64"
        chmod +x /usr/lib/docker/cli-plugins/docker-buildx
    fi
}


# Install pushrm
function setup_pushrm() {
    PUSHRM_VERSION="$_flow_build_container_pushrm_version"
    mkdir -p /usr/lib/docker/cli-plugins
    wget -q -O /usr/lib/docker/cli-plugins/docker-pushrm "${pushrm_url}/v${PUSHRM_VERSION}/docker-pushrm_linux_amd64"
    chmod +x /usr/lib/docker/cli-plugins/docker-pushrm
}

# Build to Gitlab registry
function build_local() {
    # Vars
    gitlab_registry="$CI_REGISTRY"
    gitlab_registry_image="$CI_REGISTRY_IMAGE"
    gitlab_registry_link="$CI_SERVER_PROTOCOL://$CI_SERVER_HOST/$CI_PROJECT_PATH/container_registry"

    # Add to release artifact
    mkdir -p "$_flow_reports_path/release"
    echo "  - '{\"name\":\"Gitlab Container Registry\",\"url\":\"$gitlab_registry_link\",\"type\":\"image\"}'" > "$_flow_reports_path/release/$release_link_gitlab"

    # Login and check
    docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $gitlab_registry
    docker manifest inspect $gitlab_registry_image:$build_version &> /dev/null && echo "Image exists" && return 0

    # Build and push
    if [ "$_flow_build_container_multiarch" == "true" ]; then
        docker buildx create --use
        docker buildx build --pull --platform $_flow_build_container_architectures --provenance false --tag $gitlab_registry_image:$build_version --tag $gitlab_registry_image:latest --push -f $_flow_build_container_path .
    else
        docker build --pull -t $gitlab_registry_image:$build_version -t $gitlab_registry_image:latest -f $_flow_build_container_path .
        docker push $gitlab_registry_image:$build_version
        docker push $gitlab_registry_image:latest
    fi

    # Info
    printf "Built container image: %s:%s\n" "$gitlab_registry_image" "$build_version"
    printf "Built container image: %s:latest\n" "$gitlab_registry_image"
}


# Build to Dockerhub registry
function build_dockerhub() {
    # Skip if build is false
    if [ "$_flow_build_container_dockerhub" != "true" ]; then
        return 0
    fi
    # Skip if release is not enabled
    if [ "$_flow_release" != "true" ]; then
        printf "Dockerhub image will not be built while release is not enabled.\n"
        return 0
    fi
    # Only build on production branch
    if [ "$CI_COMMIT_BRANCH" != "$_flow_prod_branch" ]; then
        return 0
    fi
    # Ensure token is set
    if [ -z "$FLOW_DOCKERHUB_TOKEN" ]; then
        printf "ERROR: Dockerhub token is empty (might be protected)\n"
        exit 1
    fi
    # Ensure user is set
    if [ "$_flow_build_container_dockerhub_user" == "unset" ]; then
        printf "ERROR: Dockerhub user is missing\n"
        exit 1
    fi
    # Ensure image name is set
    if [ "$_flow_build_container_dockerhub_image" == "unset" ]; then
        printf "ERROR: Dockerhub image is missing\n"
        exit 1
    fi
    # Ensure image title is set
    if [ "$_flow_build_container_dockerhub_title" == "unset" ]; then
        printf "ERROR: Dockerhub image title is missing\n"
        exit 1
    fi
    # Ensure image description is set
    if [ "$_flow_build_container_dockerhub_description" == "unset" ]; then
        printf "ERROR: Dockerhub image description is missing\n"
        exit 1
    fi

    # Vars
    dockerhub_registry="docker.io"
    dockerhub_registry_image="$_flow_build_container_dockerhub_user/$_flow_build_container_dockerhub_image"
    dockerhub_registry_link="https://hub.docker.com/r/${_flow_build_container_dockerhub_user}/${_flow_build_container_dockerhub_image}"

    # Setup push readme
    setup_pushrm

    # Set vars
    container_readme_file=DOCKERHUB_README.md
    title="$_flow_build_container_dockerhub_title"
    description=$(printf "%s" "$_flow_build_container_dockerhub_description" | cut -c 1-100)

    # Grab content from readme
    if [ -f $_flow_doc_readme_path ]; then
        content=$(sed -n '/<!-- CONTAINER-README-START -->/,/<!-- CONTAINER-README-END -->/p' "$_flow_doc_readme_path" | sed '/<!-- CONTAINER-README-START -->/d; /<!-- CONTAINER-README-END -->/d')
    fi

    touch "$container_readme_file"
    printf "# %s\n\n" "$title" > $container_readme_file
    printf "[Source repository](%s)\n\n" "$CI_PROJECT_URL" >> $container_readme_file
    echo "$content" >> $container_readme_file

    # Add to release artifact
    mkdir -p "$_flow_reports_path/release"
    echo "  - '{\"name\":\"Dockerhub Container Registry\",\"url\":\"$dockerhub_registry_link\",\"type\":\"image\"}'" > "$_flow_reports_path/release/$release_link_dockerhub"

    # Login and check
    docker login -u $_flow_build_container_dockerhub_user -p $FLOW_DOCKERHUB_TOKEN $dockerhub_registry
    docker manifest inspect $dockerhub_registry_image:$build_version &> /dev/null && echo "Image exists" && return 0

    # Build and push
    if [ "$_flow_build_container_multiarch" == "true" ]; then
        docker buildx create --use
        docker buildx build --pull --platform $_flow_build_container_architectures --provenance false --tag $dockerhub_registry_image:$build_version --tag $dockerhub_registry_image:latest --push -f $_flow_build_container_path .
    else
        docker build --pull -t $dockerhub_registry_image:$build_version -t $dockerhub_registry_image:latest -f $_flow_build_container_path .
        docker push $dockerhub_registry_image:$build_version
        docker push $dockerhub_registry_image:latest
    fi

    # Push readme and description
    docker pushrm -s "$description" --file $container_readme_file $dockerhub_registry_image

    # Info
    printf "Built container image: %s:%s\n" "$dockerhub_registry_image" "$build_version"
    printf "Built container image: %s:latest\n" "$dockerhub_registry_image"
}


# ------------------------------------------------------------------------------
# Run
# ------------------------------------------------------------------------------

case "$1" in
    build)
        ensure_dependencies
        setup_buildx
        build_local
        build_dockerhub
    ;;
    *)
        echo "ERROR: Invalid argument"
        exit 1
    ;;
esac
