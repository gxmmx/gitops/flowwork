#!/usr/bin/env sh

# Flow common
source /usr/bin/flow-common
codequality_report="$_flow_report_build_container_validate"

# ------------------------------------------------------------------------------
# Functions
# ------------------------------------------------------------------------------

# Ensure dockerfile exists
function ensure_buildfile() {
    if [ ! -f "${_flow_build_container_path}" ]; then
        log_error "Missing file" "Build file is missing." "Compatability" "blocker" "$_flow_build_container_path"
    fi
}


# Ensure release is enabled for external build
function ensure_release() {
    if [ "$_flow_release" != "true" ]; then
        log_error "Invalid config" "Build can not be released without enabling flow release." "Compatability" "blocker" ".gitlab-ci.yml"
    fi
}


# Ensure Dockerhub variables are set
function ensure_dockerhub_vars() {
    if [ "$_flow_build_container_dockerhub_user" == "unset" ]; then
        log_error "Invalid config" "Config is missing a value for build_container_dockerhub_user" "Compatability" "blocker" ".gitlab-ci.yml"
    fi
    if [ "$_flow_build_container_dockerhub_image" == "unset" ]; then
        log_error "Invalid config" "Config is missing a value for build_container_dockerhub_image" "Compatability" "blocker" ".gitlab-ci.yml"
    fi
    if [ "$_flow_build_container_dockerhub_title" == "unset" ]; then
        log_error "Invalid config" "Config is missing a value for build_container_dockerhub_title" "Compatability" "blocker" ".gitlab-ci.yml"
    fi
    if [ "$_flow_build_container_dockerhub_description" == "unset" ]; then
        log_error "Invalid config" "Config is missing a value for build_container_dockerhub_description" "Compatability" "blocker" ".gitlab-ci.yml"
    fi
}


# Validation for dockerhub build
function validate_dockerhub() {
    if [ "$_flow_build_container_dockerhub" != "true" ]; then
        return 0
    fi
    # ensure_release
    ensure_dockerhub_vars
}


# ------------------------------------------------------------------------------
# Run
# ------------------------------------------------------------------------------

case "$1" in
    validate)
        ensure_buildfile
        parse_config
        validate_dockerhub
        log_end
    ;;
    *)
        echo "ERROR: Invalid argument"
        exit 1
    ;;
esac
