#!/usr/bin/env sh

# ------------------------------------------------------------------------------
# Functions
# ------------------------------------------------------------------------------

# Variables
codequality_report="$_flow_report_build_container_lint"


# Lint dockerfile
function lint_dockerfile() {
    hadolint --version || { echo "ERROR: hadolint is needed for linting" && exit 1; }

    if [ ! -f "$_flow_build_container_path" ]; then
        printf "No buildfile '%s' found to lint\n" "$_flow_build_container_path"
        return 0
    fi

    mkdir -p "${_flow_reports_path}/codequality"
    
    cmd_stdout="hadolint"
    cmd_report="hadolint -f gitlab_codeclimate"

    if [ -f "${_flow_build_container_validate_config}" ]; then
        cmd_stdout="$cmd_stdout -c ${_flow_build_container_validate_config}"
        cmd_report="$cmd_report -c ${_flow_build_container_validate_config}"
    fi

    cmd_stdout="$cmd_stdout ${_flow_build_container_path}"
    cmd_report="$cmd_report ${_flow_build_container_path}"

    $cmd_report > "$_flow_reports_path/codequality/${codequality_report}"
    $cmd_stdout

    exit $?
}


# ------------------------------------------------------------------------------
# Run
# ------------------------------------------------------------------------------

case "$1" in
    lint)
        lint_dockerfile
    ;;
    *)
        echo "ERROR: Invalid argument"
        exit 1
    ;;
esac
