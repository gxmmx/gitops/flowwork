#!/usr/bin/env sh

# Flow common
source /usr/bin/flow-common

codequality_report="$_flow_report_build_tfmodule_validate"


# ------------------------------------------------------------------------------
# Functions
# ------------------------------------------------------------------------------

# Ensure dependencies
function dependencies() {
    if [ "$_flow_release" != "true" ]; then
        log_error "Invalid config" "Terraform modules can not be built if release is not scheduled" "Compatability" "blocker" ".gitlab-ci.yml"
        log_end
    fi
    if [ ! -d "$_flow_build_tfmodule_path" ]; then
        log_error "Missing dir" "Required directory is missing" "Compatibility" "major" "$_flow_build_tfmodule_path"
        log_end
    fi
    if [ "$(find "$_flow_build_tfmodule_path" -mindepth 1 -maxdepth 1 -type d | wc -l)" -eq 0 ]; then
        log_error "No module" "No Terraform module found in path" "Compatibility" "major" "$_flow_build_tfmodule_path"
        log_end
    fi
}


# Terraform format check
function terraform_format() {
    export TF_ROOT="$_flow_build_tfmodule_path/$1"
    gitlab-terraform fmt
    if [ $? -ne 0 ]; then
        for file in $(gitlab-terraform -- fmt -list -recursive); do
            log_error "Format" "Terraform file has formatting issues." "Compatability" "major" "$(basename $file)"
        done
    fi
}


# Validate modules
function validate_tfmodules() {
    for tfmodule in "$_flow_build_tfmodule_path"/*; do
        if [ -d "$tfmodule" ]; then
            module_name=$(basename "$tfmodule")
            if [ -z "$(find "$tfmodule" -maxdepth 1 -type f -name "*.tf" -print -quit)" ]; then
                log_error "Invalid module" "No Terraform files found in module $module_name" "Compatibility" "major" "$_flow_build_tfmodule_path/$module_name"
            fi
            terraform_format "$module_name"
        fi
    done
}


# ------------------------------------------------------------------------------
# Run
# ------------------------------------------------------------------------------

case "$1" in
    validate)
        dependencies
        validate_tfmodules
        log_end
    ;;
    *)
        echo "ERROR: Invalid argument"
        exit 1
    ;;
esac
