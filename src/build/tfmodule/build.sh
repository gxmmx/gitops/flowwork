#!/usr/bin/env sh

set -e

# ------------------------------------------------------------------------------
# Functions
# ------------------------------------------------------------------------------

# Build module
function build_module() {
    tfmodule=$(echo "$1" | tr " _" -)
    version="${FLOW_RELEASE_VERSION:-$CI_COMMIT_SHORT_SHA}"

    name=$(echo "$tfmodule" | rev | cut -d'-' -f2- | rev)
    system=$(echo "$tfmodule" | rev | cut -d'-' -f1 | rev)

    # Set default values if no hyphen is found
    if [ "$name" == "$tfmodule" ]; then
        system="local"
    fi

    tar -vczf /tmp/${name}-${system}-${version}.tgz -C "$_flow_build_tfmodule_path/$1" --exclude=./.git .
    curl --fail-with-body --location --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file /tmp/${name}-${system}-${version}.tgz "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/terraform/modules/${name}/${system}/${version}/file"
    printf "Terraform Module '%s-%s-%s' published\n" "$name" "$system" "$version"
}


# Build all modules
function build_modules() {
    for tfmodule in "$_flow_build_tfmodule_path"/*; do
        if [ -d "$tfmodule" ]; then
            module_name=$(basename "$tfmodule")
            build_module "$module_name"
        fi
    done
}


# ------------------------------------------------------------------------------
# Run
# ------------------------------------------------------------------------------

case "$1" in
    build)
        build_modules
    ;;
    *)
        echo "ERROR: Invalid argument"
        exit 1
    ;;
esac
