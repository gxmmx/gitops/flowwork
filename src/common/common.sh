#! /bin/sed 2,5!d;s/^#.//
# This script must be sourced from within a shell
# and not executed. For instance with:
# 
#   source /usr/bin/flow-common

# ------------------------------------------------------------------------------
# Code Quality report logger
# ------------------------------------------------------------------------------
# Logs to stdout as well as generating a codequality report.
#

# Required variables
codequality_report=""

# Internal variables
cq_error_count=0

# Log an error
# $1: Check name string
# $2: Check description
# $3: Category (Bug Risk|Clarity|Compatibility|Complexity|Duplication|Performance|Security|Style)
# $4: Severity (info|minor|major|critical|blocker)
# $5: filename
# $6: line number
function log_error() {
    check_name="$1"
    description="$2"
    category="$3"
    severity="$4"
    file="$5"
    line="$6"

    if [ -z "$codequality_report" ]; then
        printf "ERROR: codequality_report name is not set"
        exit 1
    fi
    report_file="${_flow_reports_path}/codequality/${codequality_report}"

    # init log or append next error
    if [ $cq_error_count -eq 0 ]; then
        mkdir -p "${_flow_reports_path}/codequality"
        printf "[\n" > $report_file
    else
        printf ",\n" >> $report_file
    fi

    cq_error_count=$(( cq_error_count + 1 ))

    # Fingerprint
    if [ -d "$file" ]; then
        printf '%s: %s: %s' "$file" "$check_name" "$description" > "$file/parent_dir"
        fingerprint=$(md5sum "$file/parent_dir" | cut -d ' ' -f1)
    elif [ -e "$file" ]; then
        fingerprint=$(md5sum $file | cut -d ' ' -f1)
    else
        directory=$(dirname "$file")
        mkdir -p "$directory"
        printf '%s: %s: %s' "$file" "$check_name" "$description" > "$file"
        fingerprint=$(md5sum $file | cut -d ' ' -f1)
    fi

    # Print report
    printf '  {\n    "type": "issue",\n    "check_name": "%s",\n' "$check_name" >> $report_file
    printf '    "description": "%s: %s",\n    "severity": "%s",\n' "$check_name" "$description" "$severity" >> $report_file
    printf '    "categories": ["%s"],\n    "fingerprint": "%s",\n' "$category" "$fingerprint" >> $report_file
    printf '    "location": {\n      "path": "%s"' "$file" >> $report_file
    if [ -n "$line" ]; then
        printf ',\n      "lines": {\n        "begin": %s\n      }\n    }\n' "$line" >> $report_file
    else
        printf ',\n      "lines": {\n        "begin": 0\n      }\n    }\n' >> $report_file
    fi
    printf '  }' >> $report_file

    # Print stdout
    if [ -z "$line" ]; then
        printf '%s:0 %s: %s\n' "$file" "$check_name" "$description"
    else
        printf '%s:%s %s: %s\n' "$file" "$line" "$check_name" "$description"
    fi
}

# End the log
function log_end() {
    report_file="${_flow_reports_path}/codequality/${codequality_report}"
    if [ $cq_error_count -eq 1 ]; then
        printf "\n]\n" >> $report_file
        printf "\nCodequality report\n"
        printf "Summary: 1 error\n"
        exit 1
    elif [ $cq_error_count -gt 0 ]; then
        printf "\n]\n" >> $report_file
        printf "\nCodequality report\n"
        printf "Summary: %s errors\n" "$cq_error_count"
        exit 1
    else
        mkdir -p "${_flow_reports_path}/codequality"
        printf "[]" > $report_file
        printf "\nCodequality report\n"
        printf "Summary: 0 errors\n"
        exit 0
    fi
}

# ------------------------------------------------------------------------------
# Yaml config file parser
# ------------------------------------------------------------------------------
# Parses variables in single level yaml config file.
#

# Required variables
yaml_config=""

# Parse yaml
function parse_yaml() {
    local prefix=$2
    local s='[[:space:]]*' w='[a-zA-Z0-9_]*' fs=$(echo @|tr @ '\034')
    sed -ne "s|^\($s\):|\1|" \
        -e "s|^\($s\)\($w\)$s:$s[\"']\(.*\)[\"']$s\$|\1$fs\2$fs\3|p" \
        -e "s|^\($s\)\($w\)$s:$s\(.*\)$s\$|\1$fs\2$fs\3|p"  $1 |
    awk -F$fs '{
        indent = length($1)/2;
        vname[indent] = $2;
        for (i in vname) {if (i > indent) {delete vname[i]}}
        if (length($3) > 0) {
            boolval["True"] = "true";
            boolval["true"] = "true";
            boolval["Yes"] = "true";
            boolval["yes"] = "true";
            boolval["False"] = "false";
            boolval["false"] = "false";
            boolval["No"] = "false";
            boolval["no"] = "false";
            $3 = boolval[$3] ? boolval[$3] : $3;
            vn=""; for (i=0; i<indent; i++) {vn=(vn)(vname[i])("_")}
            printf("%s%s%s=\"%s\"\n", "'$prefix'",vn, $2, $3);
      }
   }'
}


# Parse config file and expose variables.
function parse_config() {
    if [ -n "$yaml_config" ]; then
        if [ -f "$yaml_config" ]; then
            eval $(parse_yaml $yaml_config)
        fi
    fi
}


# ------------------------------------------------------------------------------
# Useful checks
# ------------------------------------------------------------------------------

# Check if file exists if file should be checked
# $1: file
# $2: "true" if should be checked
function file_exists() {
    [ "$2" != "true" ] && return 0
    if [ ! -f "$1" ]; then
        log_error "Missing file" "File is required but could not be found." "Compatability" "major" "$1"
    fi
}

# Check if directory exists if file should be checked
# $1: directory
# $2: "true" if should be checked
function dir_exists() {
    [ "$2" != "true" ] && return 0
    if [ ! -d "$1" ]; then
        log_error "Missing file" "Directory is required but could not be found." "Compatability" "major" "$1"
    fi
}
