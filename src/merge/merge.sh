#!/usr/bin/env sh

set -e

# ------------------------------------------------------------------------------
# Functions
# ------------------------------------------------------------------------------

# Check if merge source is allowed to merge to target branch
function check_merge_source() {
    if echo ",$_flow_merge_source," | grep -q ",$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME,"; then
        echo "Source branch $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME has permission to merge."
    else
        echo "ERROR: Source branch $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME does not have permission to merge."
        exit 1
    fi
}


# ------------------------------------------------------------------------------
# Run
# ------------------------------------------------------------------------------

case "$1" in
    check)
        check_merge_source
    ;;
    *)
        echo "ERROR: Invalid argument"
        exit 1
    ;;
esac
