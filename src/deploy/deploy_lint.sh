#!/usr/bin/env sh

set -e

# ------------------------------------------------------------------------------
# Functions
# ------------------------------------------------------------------------------

# Variables
codequality_report="$_flow_report_deploy_lint"

function dependencies() {
    ansible-lint --version || { echo "ERROR: ansible-lint is needed for linting" && exit 1; }
}

# Ansible lint
function ansible_lint() {
    cmd="ansible-lint -f codeclimate"

    if [ -f "${_flow_deploy_lint_config}" ]; then
        cmd="$cmd -c ${_flow_deploy_format_config}"
    fi

    cmd="$cmd ${_flow_deploy_path}"

    mkdir -p "${_flow_reports_path}/codequality"
    $cmd > "${_flow_reports_path}/codequality/${codequality_report}"
}


# ------------------------------------------------------------------------------
# Run
# ------------------------------------------------------------------------------

case "$1" in
    lint)
        dependencies
        ansible_lint
    ;;
    *)
        echo "ERROR: Invalid argument"
        exit 1
    ;;
esac
