#!/usr/bin/env python

import os
import json
import yaml


reports_path = os.environ.get("_flow_reports_path") or "ci_reports"
reports_provision_path = f"{reports_path}/provision"
deploy_path = os.environ.get("_flow_deploy_path") or "deploy"
deploy_inventory_path = os.environ.get("_flow_deploy_inventory") or "inventory"

provision_file = "output.json"
inventory_file = "99_provision_hosts.yml"
host_var_prefix = "deploy_host_"


def read_output():
    provision_output_path = f"{reports_provision_path}/{provision_file}"
    if not os.path.isfile(provision_output_path):
        return {}
    try:
        with open(provision_output_path, "r") as f:
            return json.loads(f.read())
    except:
        raise SystemExit('ERROR: Could not read provision output json')


def generate_inventory(output):
    inventory = {
        'all': {
            'hosts': {},
        }
    }
    for object in output:
        if str(object).startswith(host_var_prefix):
            name = object[12:]
            host = name
            if isinstance(output[object]['value'], str):
                name = output[object]['value']
                host = name

            if 'name' in output[object]['value'] and output[object]['value']['name']:
                name = output[object]['value']['name']
                host = name

            if 'host' in output[object]['value'] and output[object]['value']['host']:
                host = output[object]['value']['host']

            inventory['all']['hosts'][name] = {
                "ansible_host": host
            }

            if 'groups' in output[object]['value'] and output[object]['value']['groups']:
                if isinstance(output[object]['value']['groups'], list):
                    groups = output[object]['value']['groups']
                elif isinstance(output[object]['value']['groups'], str):
                    groupstring = output[object]['value']['groups']
                    groups = str(groupstring).replace(" ","").split(",")
                else:
                    raise SystemExit('ERROR: Could not parse provision output json')

                for group in groups:
                    inventory['all'].setdefault('children', {})
                    inventory['all']['children'].setdefault(group, {})
                    inventory['all']['children'][group].setdefault('hosts', {})
                    inventory['all']['children'][group]['hosts'][name] = "hostplaceholdervalue"
    return inventory


def write_inventory(inventory):
    if not inventory['all']['hosts']:
        return

    provision_inventory_dir = f"{deploy_path}/{deploy_inventory_path}"
    provision_iventory_path = f"{provision_inventory_dir}/{inventory_file}"

    if not os.path.exists(provision_inventory_dir):
        os.makedirs(provision_inventory_dir, exist_ok=True)
    
    yaml_content = "---\n" + yaml.dump(inventory, default_flow_style=False, sort_keys=False).replace(' hostplaceholdervalue', '')

    try:
        with open(provision_iventory_path, 'w') as yaml_file:
            yaml_file.write(yaml_content)
    except:
        raise SystemExit('ERROR: Could not write inventory')


def main():
    output = read_output()
    inventory = generate_inventory(output)
    write_inventory(inventory)

if __name__ == '__main__':
    main()
