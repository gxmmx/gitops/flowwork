#!/usr/bin/env sh

# Flow common
source /usr/bin/flow-common

codequality_report="$_flow_report_deploy_format"

# ------------------------------------------------------------------------------
# Functions
# ------------------------------------------------------------------------------

# Check dependencies
function dependencies() {
    yamllint --version || { echo "ERROR: yamllint is needed for format" && exit 1; }
}


# Run yamllint
function yaml_lint() {
    cmd_stdout="yamllint"
    cmd_report="yamllint -f parsable"

    if [ -f "${_flow_deploy_format_config}" ]; then
        cmd_stdout="$cmd_stdout -c ${_flow_deploy_format_config}"
        cmd_report="$cmd_report -c ${_flow_deploy_format_config}"
    fi

    cmd_stdout="$cmd_stdout ${_flow_deploy_path}"
    cmd_report="$cmd_report ${_flow_deploy_path}"

    $cmd_stdout
    if [ $? -ne 0 ]; then
        for err_line in $($cmd_report); do
            file=$(echo "$err_line" | cut -d ":" -f 1)
            line=$(echo "$err_line" | cut -d ":" -f 2)
            type=$(echo "$err_line" | awk -F'[][]' '{print $2}')
            description=$(echo "$err_line" | awk -F'] ' '{print $2}' | awk -F' [(]' '{print $1}')
            name=$(echo "$err_line" | awk -F'[(]' '{print $2}' | awk -F'[)]' '{print $1}')
            if [ "$type" == 'warning' ]; then
                severity="minor"
            else
                severity="major"
            fi
            log_error "$name" "$description" "Compatability" "$severity" "$file" "$line"
        done
    fi
}


# ------------------------------------------------------------------------------
# Run
# ------------------------------------------------------------------------------

case "$1" in
    format)
        dependencies
        yaml_lint
        log_end
    ;;
    *)
        echo "ERROR: Invalid argument"
        exit 1
    ;;
esac
