#!/usr/bin/env sh

# Flow common
source /usr/bin/flow-common

codequality_report="$_flow_report_deploy_validate"

# ------------------------------------------------------------------------------
# Functions
# ------------------------------------------------------------------------------

## Variables
inventory_dir="${_flow_deploy_path}/${_flow_deploy_inventory}"
playbook_file="${_flow_deploy_path}/${_flow_deploy_playbook}"

# Validate deployment
function validate_deployment() {
    # inventory directory
    if [ ! -d "$inventory_dir" ]; then
        log_error "Missing directory" "Inventory directory missing." "Compatability" "blocker" "$inventory_dir"
    fi

    # playbook file
    if [ ! -f "$playbook_file" ]; then
        log_error "Missing file" "Playbook file missing." "Compatability" "blocker" "$playbook_file"
    fi
}


# ------------------------------------------------------------------------------
# Run
# ------------------------------------------------------------------------------

case "$1" in
    validate)
        validate_deployment
        log_end
    ;;
    *)
        echo "ERROR: Invalid argument"
        exit 1
    ;;
esac
