#!/usr/bin/env sh

set -e

# ------------------------------------------------------------------------------
# Functions
# ------------------------------------------------------------------------------

inventory="$_flow_deploy_inventory"
playbook="$_flow_deploy_playbook"
deploy_path="$_flow_deploy_path"

# Check dependencies
function dependencies() {
    if [ ! -f "/usr/bin/flow-deploy-inventory" ]; then
        echo "ERROR: flow-deploy-inventory is needed for deployment" && exit 1
    fi
    which ssh || { echo "ERROR: openssh is needed for deployment" && exit 1; }

    # Disable host key checking for dynamic runners
    export ANSIBLE_HOST_KEY_CHECKING="False"

    # Deploy key existence
    if [ -n "$FLOW_DEPLOY_KEY" ]; then
        mkdir -p ~/.ssh/
        chmod 700 ~/.ssh
        printf "$FLOW_DEPLOY_KEY" | base64 -d | tr -d '\r' > "~/.ssh/deploy_key"
        chmod 400 "~/.ssh/deploy_key"
        export ANSIBLE_PRIVATE_KEY_FILE="~/.ssh/deploy_key"
    fi
}


# Setup ansible dependencies
function deploy_dependencies() {
    # Install requirements
    return 0
}

# Set inventory
function set_inventory() {
    /usr/bin/flow-deploy-inventory
}


# Deploy playbook
function deploy_playbook() {
    cd "${deploy_path}"
    ansible-playbook -i "$inventory" "$playbook"
}

# ------------------------------------------------------------------------------
# Run
# ------------------------------------------------------------------------------

case "$1" in
    deploy)
        dependencies
        deploy_dependencies
        set_inventory
        deploy_playbook
    ;;
    *)
        echo "ERROR: Invalid argument"
        exit 1
    ;;
esac
