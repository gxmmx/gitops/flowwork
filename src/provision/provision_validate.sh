#!/usr/bin/env sh

codequality_report="$_flow_report_provision_validate"

# ------------------------------------------------------------------------------
# Functions
# ------------------------------------------------------------------------------

# Variables
JQ_CODEQUALITY='
  (
    [.diagnostics[]]
  ) | 
    map({
      "type": "issue",
      "check_name": .summary,
      "description": .detail,
      "severity": "blocker",
      "categories": ["Compatibility"],
      "fingerprint": (.detail | @base64)[0:32],
      "location": {
        "path": (.range.filename // "terraform"),
        "lines": {
          "begin": (.range.start.line // "0")
        }
      }
    })
'

# Terraform validate
function terraform_validate() {
    mkdir -p "${_flow_reports_path}/codequality"
    gitlab-terraform validate
    if [ $? -ne 0 ]; then
        terraform validate -json | jq -r "$JQ_CODEQUALITY" > "${_flow_reports_path}/codequality/${codequality_report}"
        exit 1
    else
        printf "[]" > "${_flow_reports_path}/codequality/${codequality_report}"
        exit 0
    fi
}


# ------------------------------------------------------------------------------
# Run
# ------------------------------------------------------------------------------

case "$1" in
    validate)
        terraform_validate
    ;;
    *)
        echo "ERROR: Invalid argument"
        exit 1
    ;;
esac
