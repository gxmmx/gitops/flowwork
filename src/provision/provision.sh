#!/usr/bin/env sh

set -e

# ------------------------------------------------------------------------------
# Functions
# ------------------------------------------------------------------------------

# Variables
release_link_environment="link_environment.yml"
output_json="output.json"

# Sets environment variables passed to Terraform
function set_environment() {
    export TF_VAR_FLOW_RELEASE_VERSION="${FLOW_RELEASE_VERSION:-$CI_COMMIT_SHORT_SHA}"
    export TF_VAR_FLOW_ENVIRONMENT_NAME="${FLOW_ENVIRONMENT_NAME}"
    export TF_VAR_FLOW_ENVIRONMENT="${FLOW_ENVIRONMENT}"
    export TF_VAR_FLOW_ENVIRONMENT_SLUG="${FLOW_ENVIRONMENT_SLUG}"
    export TF_VAR_FLOW_FQDN="${FLOW_FQDN}"
    export TF_VAR_FLOW_URL="${FLOW_URL}"
}


# Terraform plan
function terraform_plan() {
    # Plan environment
    gitlab-terraform plan
    # Terraform Artifact
    gitlab-terraform plan-json
}


# Terraform apply
function terraform_apply() {
    # Apply environment
    gitlab-terraform apply

    # Release link
    mkdir -p "$_flow_reports_path/release"
    echo "  - '{\"name\":\"Environment\",\"url\":\"$FLOW_URL\",\"type\":\"other\"}'" > "$_flow_reports_path/release/$release_link_environment"

    # Output artifact
    mkdir -p "$_flow_reports_path/provision"
    gitlab-terraform output -json > "$_flow_reports_path/provision/${output_json}"
}


# Terraform destroy
function terraform_destroy() {
    # Destroy environment
    gitlab-terraform destroy
    # Delete Terraform state
    if [ -n "$FLOW_STATE_TOKEN" ]; then
        printf "State file '%s' cleanup.\n" "$TF_STATE_NAME"
        curl --header "Private-Token: ${FLOW_STATE_TOKEN}" --request DELETE "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/terraform/state/${TF_STATE_NAME}"
    fi
}


# ------------------------------------------------------------------------------
# Run
# ------------------------------------------------------------------------------

case "$1" in
    prepare)
        set_environment
        terraform_plan
    ;;
    provision)
        set_environment
        terraform_apply
    ;;
    destroy)
        set_environment
        terraform_destroy
    ;;
    *)
        echo "ERROR: Invalid argument"
        exit 1
    ;;
esac
