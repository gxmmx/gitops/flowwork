#!/usr/bin/env sh

# Flow common
source /usr/bin/flow-common

codequality_report="$_flow_report_provision_format"

# ------------------------------------------------------------------------------
# Functions
# ------------------------------------------------------------------------------

# Terraform format check
function terraform_format() {
    gitlab-terraform fmt
    if [ $? -ne 0 ]; then
        for file in $(gitlab-terraform -- fmt -check -recursive); do
            log_error "Format" "Terraform file has formatting issues." "Compatability" "major" "$file"
        done
    fi
    log_end
}


# ------------------------------------------------------------------------------
# Run
# ------------------------------------------------------------------------------

case "$1" in
    format)
        terraform_format
    ;;
    *)
        echo "ERROR: Invalid argument"
        exit 1
    ;;
esac
