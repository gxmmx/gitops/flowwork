#!/usr/bin/env sh

# Flow common
source /usr/bin/flow-common
codequality_report="$_flow_report_release_validate"

# ------------------------------------------------------------------------------
# Functions
# ------------------------------------------------------------------------------

# Variables
latest_version=""
latest_changes=""

release_envvars="${_flow_reports_path}/release/${_flow_report_release_envvars}"
release_changes="${_flow_reports_path}/release/release.md"
release_release="${_flow_reports_path}/release/release.yml"


# Validate dependencies
function dependencies() {
    # Check release-cli
    release-cli --version &> /dev/null || { echo "ERROR: release-cli is needed for release" && exit 1; }
}


# Lint changelog
function lint_changelog() {
    # Do not lint if changelog does not exist
    # Check changelog existence
    if [ ! -f "$_flow_doc_changelog_path" ]; then
        log_error "Missing file" "Changelog file is missing from repository." "Compatability" "blocker" "$_flow_doc_changelog_path"
        return 0
    fi

    # Check nonstandard spaces (NBSP, NNBSP, ZWSP)
    if grep -e $'\xC2\xA0' -e $'\xE2\x80\xAF' -e $'\xE2\x80\x8B' $_flow_doc_changelog_path &> /dev/null; then
        log_error "Nonstandard spaces" "Changelog contains nonstandard spaces or tabs" "Compatibility" "major" "$_flow_doc_changelog_path"
    fi

    # Check if changelog has version line
    if ! grep -E '^## \[' "$_flow_doc_changelog_path" &> /dev/null; then
        log_error "No version" "Changelog does not contain a valid version declaration" "Compatibility" "major" "$_flow_doc_changelog_path"
    fi

    # Check version lines format
    if grep -E '^## \[' "$_flow_doc_changelog_path" 2>/dev/null | grep -E -v '^## \[[0-9]+\.[0-9]+\.[0-9]+\] - [0-9]{4}-[0-9]{2}-[0-9]{2}$'; then
        log_error "Invalid version" "Changelog contains an invalid version declaration" "Compatibility" "major" "$_flow_doc_changelog_path"
    fi

    # Check version line dates
    grep -E '^## \[' "$_flow_doc_changelog_path" 2>/dev/null | grep -E '^## \[[0-9]+\.[0-9]+\.[0-9]+\] - [0-9]{4}-[0-9]{2}-[0-9]{2}$' | awk '{print $4}' | while IFS= read -r date; do
        if ! date -d "$date" >/dev/null 2>&1; then
            log_error "Invalid date" "Changelog contains an invalid date declaration: $date" "Compatibility" "major" "$_flow_doc_changelog_path"
        fi
    done

    # Check duplicate versions
    duplicate_versions=$(grep -E '^## \[' "$_flow_doc_changelog_path" 2>/dev/null | grep -E '^## \[[0-9]+\.[0-9]+\.[0-9]+\] - [0-9]{4}-[0-9]{2}-[0-9]{2}$' | uniq -d)
    if [ "${duplicate_versions}" != "" ]; then
        log_error "Duplicate version" "Changelog contains duplicate version declarations" "Compatibility" "major" "$_flow_doc_changelog_path"
    fi

    # Check latest version
    latest_version=$(awk -F '[][]' '{ for (i = 1; i <= NF; i++) if ($i ~ /^[0-9]+\.[0-9]+\.[0-9]+$/) { print $i; exit; } }' $_flow_doc_changelog_path)
    if [ -z $latest_version ]; then
        log_error "No version" "Changelog could not parse latest version" "Compatibility" "major" "$_flow_doc_changelog_path"
    fi
}

# Changelog latest version
function get_latest_version() {
    # Do not fetch version if changelog does not exist
    if [ ! -f "$_flow_doc_changelog_path" ]; then
        return 0
    fi

    latest_version=$(awk -F '[][]' '{ for (i = 1; i <= NF; i++) if ($i ~ /^[0-9]+\.[0-9]+\.[0-9]+$/) { print $i; exit; } }' $_flow_doc_changelog_path)
    if [ -z $latest_version ]; then
        echo "ERROR: No valid version found in changelog."
        exit 1
    fi
    printf "Preparing release for v%s\n" "${latest_version}"
}

# Changelog latest changes
function get_latest_changes() {
    # Do not fetch changes if changelog does not exist
    if [ ! -f "$_flow_doc_changelog_path" ]; then
        return 0
    fi

    latest_changes=$(awk -v ver="[$latest_version]" '
    /^## / { if (p) { exit }; if ($2 == ver) { p=1; next } } p && NF
    ' "$_flow_doc_changelog_path")
    printf "Preparing release changes for v%s\n" "${latest_version}"
}

# Check if release exists for latest version
function check_latest_release() {
    #current_tag=$(git describe --abbrev=0 2>/dev/null || echo "")
    #current_version=$(echo "$current_tag" | awk -F 'v' '{ if ($2 ~ /^([0-9]+)\.([0-9]+)\.([0-9]+)$/) print $2; else print "" }')
    release_info=$(release-cli get --tag-name "v${latest_version}" 2>&1)
    if ! echo "$release_info" | grep "404 Not Found" &>/dev/null; then
        log_error "Release exists" "There is already a release for $latest_version" "Compatibility" "major" "$_flow_doc_changelog_path"
    fi
}


# Set release artifacts
function set_release_artifacts() {
    # Release env
    mkdir -p $(dirname "$release_envvars")
    echo "FLOW_RELEASE_VERSION=${latest_version}" > $release_envvars
    echo "FLOW_RELEASE_TAG=v${latest_version}" >> $release_envvars
    printf "Release environment vars '%s' generated\n" "$release_envvars"

    # Release changes
    mkdir -p $(dirname "$release_changes")
    echo "$latest_changes" > $release_changes
    printf "Release changes '%s' generated\n" "$release_changes"

    # Release file
    mkdir -p $(dirname "$release_release")
    printf "---\n" > $release_release
    printf "name: Version %s\n" "$latest_version" >> $release_release
    printf "description: %s\n" "$release_changes" >> $release_release
    printf "tag-name: v%s\n" "${latest_version}" >> $release_release
    printf "ref: %s\n" "$CI_COMMIT_SHA" >> $release_release
    # Project link
    printf "assets-link:\n" >> $release_release
    echo "  - '{\"name\":\"Project\",\"url\":\"$CI_PROJECT_URL\",\"type\":\"other\"}'" >> $release_release
    printf "Release file '%s' generated\n" "$release_release"
}


# Add links from other jobs to release artifacts
function add_release_links() {
    release_links=$(find "${_flow_reports_path}/release" -iname link* 2> /dev/null)
    if [ -n "$release_links" ]; then
        while IFS= read -r file; do
            cat "$file" >> $release_release
            printf "Release link '%s' added to release file\n" "$file"
        done < <(echo "$release_links")
    fi
}

# Cut release for latest version
function create_release() {
    if [ ! -f $release_release ]; then
        echo "ERROR: Release artifact has not been set by prepare job"
        exit 1
    fi

    release-cli create-from-file --file $release_release
}


# ------------------------------------------------------------------------------
# Run
# ------------------------------------------------------------------------------

case "$1" in
    validate)
        dependencies
        lint_changelog
        check_latest_release
        log_end
    ;;
    prepare)
        dependencies
        get_latest_version
        get_latest_changes
        set_release_artifacts
    ;;
    release)
        dependencies
        add_release_links
        create_release
    ;;
    *)
        echo "ERROR: Invalid argument"
        exit 1
    ;;
esac
