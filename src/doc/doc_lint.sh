#!/usr/bin/env sh

# ------------------------------------------------------------------------------
# Functions
# ------------------------------------------------------------------------------

# Variables
codequality_report="$_flow_report_doc_lint"

config_file_used="$_flow_doc_lint_config"
config_file_yaml=".markdownlint.yaml"
config_file_cli2=".markdownlint-cli2.yaml"


# Set markdownlint config
function set_markdown_config() {
    # CLI2 config
    mkdir -p "${_flow_reports_path}/codequality"
    cli2_content=$(cat <<EOF
config:
  default: true
noInlineConfig: false
fix: false
outputFormatters:
  - [ "markdownlint-cli2-formatter-codequality", { "name": "${_flow_reports_path}/codequality/${codequality_report}" } ]
  - [ "markdownlint-cli2-formatter-default" ]
EOF
    )

    echo "$cli2_content" > $config_file_cli2

    # Custom markdownlint config
    if [ -f $config_file_used ]; then
        if [ "$config_file_used" != "$config_file_yaml" ]; then
            mv "$config_file_used" "$config_file_yaml"
        fi
    fi
}


# Validate and lint markdown
function validate_markdown() {
    markdownlint-cli2 --version &> /dev/null || { echo "ERROR: markdownlint is needed for linting" && exit 1; }

    # Run lint
    markdownlint-cli2 "**.md" "#node_modules"
}


# ------------------------------------------------------------------------------
# Run
# ------------------------------------------------------------------------------

case "$1" in
    lint)
        set_markdown_config
        validate_markdown
    ;;
    *)
        echo "ERROR: Invalid argument"
        exit 1
    ;;
esac
