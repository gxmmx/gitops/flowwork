#!/usr/bin/env sh

# Flow common
source /usr/bin/flow-common

codequality_report="$_flow_report_doc_validate"

# ------------------------------------------------------------------------------
# Functions
# ------------------------------------------------------------------------------

# Validate doc exists
function validate_doc_exists() {
    # Check if doc directory exists
    if [ ! -e "$_flow_doc_path" ]; then
        log_error "Missing dir" "Required directory is missing" "Compatibility" "major" "$_flow_doc_path"
    elif [ ! -d "$_flow_doc_path" ]; then
        log_error "Invalid dir" "$_flow_doc_path is not a directory" "Compatibility" "major" "$_flow_doc_path"
    fi
}

# Validate doc does not contain executable files
function validate_doc_exec() {
    exec_files=$(find "$_flow_doc_path" -type f -executable 2> /dev/null)
    if [ -n "$exec_files" ]; then
        while IFS= read -r file; do
            log_error "Executable doc" "A file in the doc path is executable. No content in doc should be executable." "Compatability" "major" "$file"
        done < <(echo "$exec_files")
    fi
}


# Validate readme
function validate_doc_readme() {
    # Validate readme exists
    if [ ! -f "$_flow_doc_readme_path" ]; then
        log_error "Missing file" "The project readme is missing from project root." "Compatability" "major" "$_flow_doc_readme_path"
    fi

    # Validate that no home* file exists in docs
    home_result=$(find "$_flow_doc_path" -iname home* 2> /dev/null)
    if [ -n "$home_result" ]; then
        while IFS= read -r file; do
            log_error "Home file existance" "Home file found in doc. Should not exist and be created from readme." "Compatability" "major" "$file"
        done < <(echo "$home_result")
    fi
}


# Check that every directory in path has a corresponding md file
function check_md_for_dir() {
    local dir="$1"
    if [ ! -d "$dir" ]; then
        return 0
    fi
    for folder in "$dir"/*/; do
        [ "$folder" = "$dir/*/" ] && continue
        folder_name=$(basename "$folder")
        if [ ! -f "$dir/${folder_name}.md" ]; then
            log_error "Missing file" "Each doc directory should have a corresponding .md file." "Compatability" "major" "$dir/${folder_name}.md"
        fi
        # Recurse
        check_md_for_dir "$folder"
    done
}


# Validate folder md files
function validate_folder_md() {
    check_md_for_dir "$_flow_doc_path"
}


# ------------------------------------------------------------------------------
# Run
# ------------------------------------------------------------------------------

case "$1" in
    validate)
        validate_doc_exists
        validate_doc_exec
        validate_doc_readme
        validate_folder_md
        log_end
    ;;
    *)
        echo "ERROR: Invalid argument"
        exit 1
    ;;
esac
