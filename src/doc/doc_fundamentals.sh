#!/usr/bin/env sh

# Flow common
source /usr/bin/flow-common

yaml_config="$_flow_doc_fundamentals_config"
codequality_report="$_flow_report_doc_fundamentals"


# ------------------------------------------------------------------------------
# Functions
# ------------------------------------------------------------------------------

# Validate fundamental files
function fundamentals_validate() {
    # Check file existance
    file_exists "$_flow_doc_readme_path" "${readme:-true}"
    file_exists "$_flow_doc_changelog_path" "${changelog:-true}"
    file_exists "$_flow_doc_license_path" "${license:-true}"
    file_exists "$_flow_doc_codeofconduct_path" "${codeofconduct:-true}"
    file_exists "$_flow_doc_contributing_path" "${contributing:-true}"
    file_exists "$_flow_doc_codeowners_path" "${codeowners:-true}"
    file_exists ".gitignore" "${gitignore:-true}"
}


# ------------------------------------------------------------------------------
# Run
# ------------------------------------------------------------------------------

case "$1" in
    validate)
        parse_config
        fundamentals_validate
        log_end
    ;;
    *)
        echo "ERROR: Invalid argument"
        exit 1
    ;;
esac
