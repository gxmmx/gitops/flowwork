#!/usr/bin/env sh

set -e

# ------------------------------------------------------------------------------
# Functions
# ------------------------------------------------------------------------------

# Variables
# Wiki uses a passed PAT, while Gitlab hasn't given CI_JOB_TOKEN wiki access (https://docs.gitlab.com/ee/ci/jobs/ci_job_token.html)
# wiki_url="${CI_SERVER_PROTOCOL}://project_${CI_PROJECT_ID}_bot:${CI_JOB_TOKEN}@${CI_SERVER_HOST}:${CI_SERVER_PORT}/${CI_PROJECT_PATH}.wiki.git"
wiki_url="${CI_SERVER_PROTOCOL}://project_${CI_PROJECT_ID}_bot:${FLOW_WIKI_TOKEN}@${CI_SERVER_HOST}:${CI_SERVER_PORT}/${CI_PROJECT_PATH}.wiki.git"
wiki_path="${CI_PROJECT_NAME}.wiki"
wiki_version="$FLOW_RELEASE_VERSION"
wiki_commit="Flow wiki update"


# Ensure git is available on runner
function ensure_git() {
    git --version &> /dev/null || { echo "ERROR: git is needed for wiki release" && exit 1; }
}


# Ensure token is set
function ensure_token() {
    if [ -z "$FLOW_WIKI_TOKEN" ]; then
        printf "ERROR: Wiki authentication token is empty.\n"
        printf "Possible 'protected' token being used on non protected branch/tag\n"
        exit 1
    fi
}


# Ensure wiki content
function ensure_content() {
    if [ ! -d "$_flow_doc_path" ]; then
        printf "ERROR: Doc directory '%s' missing.\n" "$_flow_doc_path"
        exit 1
    fi

    if [ ! -f "$_flow_doc_readme_path" ]; then
        printf "ERROR: Readme file '%s' missing.\n" "$_flow_doc_readme_path"
        exit 1
    fi
}


# Set version if wiki should be versions
function set_wiki_version() {
    if [ -n "$wiki_version" ]; then
        wiki_commit="${wiki_version}"
    fi
}


# Update wiki
function update_wiki() {
    # Clone wiki and clear
    git clone "${wiki_url}"
    cd "$CI_PROJECT_DIR/$wiki_path"
    git rm -rf . 2> /dev/null || true

    # Update bot config
    git config user.name "$CI_PROJECT_NAME Wiki"
    git config user.email "$GITLAB_USER_EMAIL"

    # Add doc
    cp "${CI_PROJECT_DIR}/${_flow_doc_readme_path}" "${CI_PROJECT_DIR}/${wiki_path}/home.md"
    cp -R "${CI_PROJECT_DIR}/${_flow_doc_path}/." "${CI_PROJECT_DIR}/${wiki_path}/"

    # Push
    git add .
    if [ -z "$(git status --porcelain)" ]; then
        echo "No changes to wiki."
    else
        git commit -m "$wiki_commit"
        git push origin HEAD:$CI_DEFAULT_BRANCH
    fi
}


# ------------------------------------------------------------------------------
# Run
# ------------------------------------------------------------------------------

case "$1" in
    release)
        ensure_git
        ensure_token
        ensure_content
        set_wiki_version
        update_wiki
    ;;
    *)
        echo "ERROR: Invalid argument"
        exit 1
    ;;
esac
